package co.imagendigital.oreo.oreoticones.content;

import java.util.ArrayList;

import android.graphics.Bitmap;

public class Emoticon {

	private String id;
	private String name;
	private String url;
	private String urlIcon;
	private String description;
	private Bitmap bitmap;
	private Bitmap bitmapIcon;
	private String fileType;
	private String idCategory;
	private ArrayList<String> category;
	
	public Emoticon(String id, String name, String url, String description, String fileType, ArrayList<String> category, String urlIcon) {
		this.id = id;
		this.name = name;
		this.url = url;
		this.urlIcon = urlIcon;
		this.description = description;
		this.fileType = fileType;
		this.category = category;
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Bitmap getBitmap() {
		return bitmap;
	}

	public void setBitmap(Bitmap bitmap) {
		this.bitmap = bitmap;
	}

	public String getFileType() {
		return fileType;
	}

	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUrlIcon() {
		return urlIcon;
	}

	public void setUrlIcon(String urlIcon) {
		this.urlIcon = urlIcon;
	}

	public Bitmap getBitmapIcon() {
		return bitmapIcon;
	}

	public void setBitmapIcon(Bitmap bitmapIcon) {
		this.bitmapIcon = bitmapIcon;
	}

	public ArrayList<String> getCategory() {
		return category;
	}

	public void setCategory(ArrayList<String> category) {
		this.category = category;
	}
	
}
