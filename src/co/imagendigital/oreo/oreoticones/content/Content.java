package co.imagendigital.oreo.oreoticones.content;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Content {

	/**
	 * An array of Emoticon.
	 */
	private static List<Emoticon> EMOTICON_LIST = new ArrayList<Emoticon>();

	/**
	 * An array of Recent Emoticon.
	 */
	private static List<Emoticon> RECENT_EMOTICON_LIST = new ArrayList<Emoticon>();
	
	
	/**
	 * An array of Emoticon.
	 */
	private static List<Category> CATEGORY_LIST = new ArrayList<Category>();
	
//	/**
//	 * Count category
//	 */
//	private static int NUMBER_OF_CATEGORY = 0;
	
	/**
	 * initialize all list 
	 */
	public static void cleanAll() {
		EMOTICON_LIST = new ArrayList<Emoticon>();
	}

	public static List<Emoticon> getEMOTICON_LIST() {
		return EMOTICON_LIST;
	}

	public static void setEMOTICON_LIST(List<Emoticon> eMOTICON_LIST) {
		EMOTICON_LIST = eMOTICON_LIST;
	}

	public static List<Category> getCATEGORY_LIST() {
		return CATEGORY_LIST;
	}

	public static void setCATEGORY_LIST(List<Category> cATEGORY_LIST) {
		CATEGORY_LIST = cATEGORY_LIST;
	}

	public static List<Emoticon> getRECENT_EMOTICON_LIST() {
		return RECENT_EMOTICON_LIST;
	}

	public static void setRECENT_EMOTICON_LIST(List<Emoticon> rECENT_EMOTICON_LIST) {
		RECENT_EMOTICON_LIST = rECENT_EMOTICON_LIST;
	}
	
	
	
}
