package co.imagendigital.oreo.oreoticones;

import java.util.ArrayList;
import java.util.List;


import co.imagendigital.oreo.oreoticones.EmoticonsGridAdapter.KeyClickListener;
import co.imagendigital.oreo.oreoticones.content.Content;
import co.imagendigital.oreo.oreoticones.content.Emoticon;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.util.AttributeSet;
import android.util.Log;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Resources;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore.Images;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;


public class MainActivity extends ActionBarActivity implements KeyClickListener{

	ProgressBar progressBar;
	EmoticonsPagerAdapter adapter;
	ViewPager pager;
	SharedPreferences sPrefs;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.emoticons_popup);

		sPrefs = PreferenceManager.getDefaultSharedPreferences(this);

		progressBar = (ProgressBar) findViewById(R.id.progressBar1);
		progressBar.setVisibility(View.GONE);
		enablePopUpView();
		setNotificationApp();
		getRecentEmoticons();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Permanent Notification 
	 */
	private void setNotificationApp() {
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(MainActivity.this)
		.setSmallIcon(R.drawable.ic_launcher)
		.setLargeIcon((((BitmapDrawable)getResources().getDrawable(R.drawable.ic_launcher)).getBitmap()))
		.setContentTitle((String)getResources().getString(R.string.app_name))
		.setContentText((String)getResources().getString(R.string.msg_notification))
		.setOngoing(true);
		Intent notIntent = new Intent(MainActivity.this, SplashScreenActivity.class);

		PendingIntent contIntent = PendingIntent.getActivity(MainActivity.this, 0, notIntent, 0);
		mBuilder.setContentIntent(contIntent);

		NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		int NOTIF_ALERTA_ID = 0;
		mNotificationManager.notify(NOTIF_ALERTA_ID, mBuilder.build());
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		/*if (id == R.id.action_settings) {
			return true;
		}*/
		return super.onOptionsItemSelected(item);
	}

	/**
	 * When the user requests a file, send an Intent to the
	 * server app.
	 * files.
	 */
	public void requestFile(Uri imageUri) {

		Intent mainIntent = new Intent();
		mainIntent.setAction(Intent.ACTION_SEND);
		mainIntent.putExtra(Intent.EXTRA_STREAM, imageUri);
		mainIntent.setType("image/*");

		PackageManager pm = getPackageManager();
		Intent sendIntent = new Intent(Intent.ACTION_SEND);     
		sendIntent.setType("text/plain");

		Intent openInChooser = Intent.createChooser(mainIntent, "Share image using");

		List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
		List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();    

		for (int i = 0; i < resInfo.size(); i++) {
			// Extract the label, append it, and repackage it in a LabeledIntent
			ResolveInfo ri = resInfo.get(i);
			String packageName = ri.activityInfo.packageName;
			if(packageName.contains("android.email")) {
				mainIntent.setPackage(packageName);
			} else if(packageName.contains("twitter") || packageName.contains("facebook") || packageName.contains("mms") || packageName.contains("android.gm") || packageName.contains("com.whatsapp")) {
				Intent intent = new Intent();
				intent.setComponent(new ComponentName(packageName, ri.activityInfo.name));
				intent.setAction(Intent.ACTION_SEND);
				intent.putExtra(Intent.EXTRA_STREAM, imageUri);
				intent.setType("image/*");
				if(packageName.contains("twitter")) {
					intent.putExtra(Intent.EXTRA_STREAM, imageUri);
				} else if(packageName.contains("facebook")) {
					intent.putExtra(Intent.EXTRA_STREAM, imageUri);
				} else if(packageName.contains("mms")) {
					intent.putExtra(Intent.EXTRA_STREAM, imageUri);
				} else if(packageName.contains("android.gm")) {
					intent.putExtra(Intent.EXTRA_STREAM, imageUri);
				}

				intentList.add(new LabeledIntent(intent, packageName, ri.loadLabel(pm), ri.icon));
			}
		}

		// convert intentList to array
		LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);

		openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
		startActivity(openInChooser);       
	}

	/**
	 * Defining all components of emoticons keyboard
	 */
	private void enablePopUpView() {

		pager = (ViewPager) findViewById(R.id.emoticons_pager);
		pager.setOffscreenPageLimit(5);


		ArrayList<String> paths = new ArrayList<String>();

		adapter = new EmoticonsPagerAdapter(MainActivity.this, paths, this);
		adapter.notifyDataSetChanged();
		pager.setAdapter(adapter);
	}

	@Override
	public void keyClickedIndex(final String index) {

		Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + getResources().getResourcePackageName(R.drawable.regalo) 
				+ '/' + getResources().getResourceTypeName(R.drawable.regalo) + '/' + getResources().getResourceEntryName(R.drawable.regalo) );


		for (int i = 0; i < Content.getEMOTICON_LIST().size(); i++) {

			if(Content.getEMOTICON_LIST().get(i).getUrlIcon().equals(index)){
				if (Content.getEMOTICON_LIST().get(i).getBitmap() == null) {
					DownloadBitmapEmoticon dbe = new DownloadBitmapEmoticon(Content.getEMOTICON_LIST().get(i), null, progressBar, this);
					dbe.execute(null,Content.getEMOTICON_LIST().get(i).getUrl());
				}else{					
					String pathofBmp = Images.Media.insertImage(getContentResolver(), Content.getEMOTICON_LIST().get(i).getBitmap(), Content.getEMOTICON_LIST().get(i).getName(), null);
					imageUri = Uri.parse(pathofBmp);
					addRecentEmoticon(Content.getEMOTICON_LIST().get(i));
					setRecentEmoticons(Content.getEMOTICON_LIST().get(i));
					requestFile(imageUri);
				}
			}
		}
	}

	public void addRecentEmoticon(Emoticon object){
		boolean exist = false;
		for (int i = 0; i < Content.getRECENT_EMOTICON_LIST().size(); i++) {
			if (Content.getRECENT_EMOTICON_LIST().get(i).getId().equals(object.getId())) {
				exist = true;
			}
		}
		if (!exist) {
			Content.getRECENT_EMOTICON_LIST().add(object);	
			
		}	
		enablePopUpView();
		//		getRecentEmoticons();
		//		adapter.updateRecentEmoticons();
	}

	
	private void setRecentEmoticons(Emoticon object) {
		SharedPreferences.Editor sEdit=sPrefs.edit();
		Log.d("IARL", "IARL set size.. "+Content.getRECENT_EMOTICON_LIST().size());
		Log.d("IARL", "IARL set content.. "+object.getId());
		sEdit.putInt("size",Content.getRECENT_EMOTICON_LIST().size());
		sEdit.putString("val"+Content.getRECENT_EMOTICON_LIST().size(),object.getId());
		sEdit.commit();
	}
	private void getRecentEmoticons() {
		ArrayList<String> myAList=new ArrayList<String>();
		int size = sPrefs.getInt("size",0);
		Log.d("IARL", "IARL leyendo size.. "+size);

		for(int j=0; j <= size; j++){
			myAList.add(sPrefs.getString("val"+j,""));
//		}
//
//		for (int j = 0; j < myAList.size(); j++) {
			Log.d("IARL", "IARL leyendo myAList.. "+myAList.get(j));
			for (int i = 0; i < Content.getEMOTICON_LIST().size(); i++) {
				if (Content.getEMOTICON_LIST().get(i).getId().equals(myAList.get(j))) {
					Log.d("IARL","iarl leyendo myRList "+Content.getEMOTICON_LIST().get(i).getId());
					addRecentEmoticon(Content.getEMOTICON_LIST().get(i));
				}
			}
		}		

	}
}
