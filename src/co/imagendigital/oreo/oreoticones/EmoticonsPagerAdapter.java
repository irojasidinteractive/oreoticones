package co.imagendigital.oreo.oreoticones;

import java.util.ArrayList;

import co.imagendigital.oreo.oreoticones.EmoticonsGridAdapter.KeyClickListener;
import co.imagendigital.oreo.oreoticones.content.Content;
import co.imagendigital.oreo.oreoticones.content.Emoticon;


import android.inputmethodservice.InputMethodService;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.View;
import android.widget.GridView;
import android.widget.TextView;

public class EmoticonsPagerAdapter extends PagerAdapter {

	private static final int NO_OF_EMOTICONS_PER_PAGE = 60;
	ActionBarActivity mActivity;
	KeyClickListener mListener;
	public EmoticonsGridAdapter adapter;

	private ArrayList<Emoticon> emoticonsInAPage;
	private ArrayList<Emoticon> recentEmoticonsInAPage;
	GridView grid;
	public EmoticonsPagerAdapter(ActionBarActivity activity, ArrayList<String> emoticons, KeyClickListener listener) {
		this.mActivity = activity;
		this.mListener = listener;

	}

	@Override
	public int getCount() {
		return (int) Math.ceil((double) Content.getCATEGORY_LIST().size() );
//		return (int) Math.ceil((double) Content.getEMOTICON_LIST().size() / (double) NO_OF_EMOTICONS_PER_PAGE);
	}

	@Override
	public Object instantiateItem(View collection, int position) {

		View layout = mActivity.getLayoutInflater().inflate(R.layout.emoticons_grid, null);
		TextView title = (TextView) layout.findViewById(R.id.title_category);

		emoticonsInAPage = new ArrayList<Emoticon>();
		recentEmoticonsInAPage = new ArrayList<Emoticon>();

		grid = (GridView) layout.findViewById(R.id.emoticons_grid);

		selectEmoticons(position);
		
		if (Content.getCATEGORY_LIST().get(position) != null) {
			title.setText(Content.getCATEGORY_LIST().get(position).getDescription());
		}
		if (position == 0) {
			adapter = new EmoticonsGridAdapter(mActivity.getApplicationContext(), Content.getRECENT_EMOTICON_LIST(), position,mListener);			
//			adapter = new EmoticonsGridAdapter(mActivity.getApplicationContext(), new ArrayList<Emoticon>(Content.getRECENT_EMOTICON_LIST()), position,mListener);			
		}else{
			adapter = new EmoticonsGridAdapter(mActivity.getApplicationContext(), emoticonsInAPage, position,mListener);
		}
		grid.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		((ViewPager) collection).addView(layout);

		return layout;
	}

	public void updateRecentEmoticons() {
		adapter = new EmoticonsGridAdapter(mActivity.getApplicationContext(), Content.getRECENT_EMOTICON_LIST(), 0, mListener);			

		grid.setAdapter(adapter);
		adapter.notifyDataSetChanged();
		//((ViewPager) collection).addView(layout);


	}
	
	private void selectEmoticons(int position) {
		/*if (position == 0) {
			for (int i = 0; i < Content.getRECENT_EMOTICON_LIST().size(); i++) {
				emoticonsInAPage.add(Content.getRECENT_EMOTICON_LIST().get(i));
			}
		}else{*/
			for (int i = 0; i < Content.getEMOTICON_LIST().size(); i++) {
				
				for (int j = 0; j < Content.getEMOTICON_LIST().get(i).getCategory().size(); j++) {
					if (Content.getCATEGORY_LIST().get(position).getId().equals(Content.getEMOTICON_LIST().get(i).getCategory().get(j))) {
						emoticonsInAPage.add(Content.getEMOTICON_LIST().get(i));
					}
				}
			}
//		}
	}
	
	@Override
	public void destroyItem(View collection, int position, Object view) {
		((ViewPager) collection).removeView((View) view);
	}

	@Override
	public boolean isViewFromObject(View view, Object object) {
		return view == object;
	}
}