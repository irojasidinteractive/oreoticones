package co.imagendigital.oreo.oreoticones;

import java.io.InputStream;
import java.util.ArrayList;

import co.imagendigital.oreo.oreoticones.content.Emoticon;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;

class DownloadBitmapEmoticon extends AsyncTask<String, Void, ArrayList<Bitmap>> {
	Emoticon dumI;
	ImageView imageBack;
	Context mContext;
	ProgressBar progressBar;

	public DownloadBitmapEmoticon(Emoticon dummyItem, ImageView imageBack, ProgressBar progressBar, Context mContext) {
		this.dumI 			= dummyItem;
		this.imageBack 		= imageBack;
		this.mContext 		= mContext;
		this.progressBar 	= progressBar;
		progressBar.setVisibility(View.VISIBLE);

	}

	@Override
	protected ArrayList<Bitmap> doInBackground(String... urls) {
		//Log.d("IARL","download1 "+urls[0]); 
		String urlIcon = urls[0];
		String urlImage = urls[1];
		Bitmap mIcon = null;
		Bitmap mImage = null;
		ArrayList<Bitmap> images = new ArrayList<Bitmap>();


		if (urlIcon == null) {
			try {		
				InputStream in2 = new java.net.URL(urlImage).openStream();
				mImage = BitmapFactory.decodeStream(in2);			
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
		}else{
			try {
				InputStream in = new java.net.URL(urlIcon).openStream();
				mIcon = BitmapFactory.decodeStream(in);			
//				InputStream in2 = new java.net.URL(urlImage).openStream();
//				mImage = BitmapFactory.decodeStream(in2);			
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
		}
		images.add(mIcon);
		images.add(mImage);
		return images;

	}

	@Override
	protected void onPostExecute(ArrayList<Bitmap> result) {
		super.onPostExecute(result);
		Log.d("IARL", "stopAsyncTask onPostExecute" );

		if (result.get(1) != null) {
			dumI.setBitmap(result.get(1));
		}
		if (result.get(0) != null) {
			dumI.setBitmapIcon(result.get(0));
		}
		if (imageBack != null) {
			imageBack.setImageBitmap(result.get(0));	
			progressBar.setVisibility(View.GONE);
			imageBack.setVisibility(View.VISIBLE);
		}else{
			progressBar.setVisibility(View.GONE);

		}


	}

}


