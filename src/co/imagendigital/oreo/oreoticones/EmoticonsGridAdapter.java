package co.imagendigital.oreo.oreoticones;

import gif.decoder.GifRun;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import co.imagendigital.oreo.oreoticones.content.Content;
import co.imagendigital.oreo.oreoticones.content.Emoticon;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class EmoticonsGridAdapter extends BaseAdapter{

	private ArrayList<Emoticon> paths;
	private int pageNumber;
	Context mContext;
	ImageView image;
	KeyClickListener mListener;
	ProgressBar progressBar;

	public EmoticonsGridAdapter(Context context, ArrayList<Emoticon> paths, int pageNumber, KeyClickListener listener) {
		this.mContext = context;
		this.paths = paths;
		this.pageNumber = pageNumber;
		this.mListener = listener;
	}
	public EmoticonsGridAdapter(Context context, List<Emoticon> paths, int pageNumber, KeyClickListener listener) {
		this.mContext = context;
		this.paths = new ArrayList<Emoticon>(paths);
		this.pageNumber = pageNumber;
		this.mListener = listener;
	}
	public View getView(int position, View convertView, ViewGroup parent){

		View v = convertView;
		if (v == null) {
			LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			v = inflater.inflate(R.layout.emoticons_item, null);			
		}

		final String pathIcon = paths.get(position).getUrlIcon();
		final String pathImage = paths.get(position).getUrl();
		Emoticon emoticon = Content.getEMOTICON_LIST().get(position);
		image = (ImageView) v.findViewById(R.id.item);
		progressBar = (ProgressBar) v.findViewById(R.id.progressBar1);
		SurfaceView imageSurface = (SurfaceView) v.findViewById(R.id.itemSurface);

		image.setVisibility(View.VISIBLE);
		Log.d(Oreo.TAG_TEMP, "emot grid adap");
		imageSurface.setVisibility(View.GONE);

		DownloadBitmapEmoticon dbe = new DownloadBitmapEmoticon(emoticon, image, progressBar, mContext);
		dbe.execute(pathIcon,pathImage);	

		//		getImage(path);
		//		image.setImageBitmap(getImage(path));

		image.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {				
				mListener.keyClickedIndex(pathIcon);
			}
		});
		imageSurface.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {				
				mListener.keyClickedIndex(pathIcon);
			}
		});

		return v;
	}

	@Override
	public int getCount() {		
		return paths.size();
	}

	@Override
	public String getItem(int position) {		
		return paths.get(position).getUrl();
	}

	@Override
	public long getItemId(int position) {		
		return position;
	}

	public int getPageNumber () {
		return pageNumber;
	}

	//	private Bitmap getImage (String path) {
	//		image.setVisibility(View.GONE);
	//		Bitmap mIcon11 = null;
	//		try {
	//			InputStream in = new java.net.URL(path).openStream();
	//			mIcon11 = BitmapFactory.decodeStream(in);			
	//		} catch (Exception e) {
	//			Log.e(Oreo.TAG_TEMP, "Error "+ e.getMessage());
	//			e.printStackTrace();
	//		}
	//		image.setImageBitmap(mIcon11);
	//		image.setVisibility(View.VISIBLE);
	//		return mIcon11;
	//	}

	//	private Bitmap getImage (String path) {
	//		AssetManager mngr = mContext.getAssets();
	//		InputStream in = null;
	//
	//		try {
	//			in = mngr.open("emoticons/" + path);
	//		} catch (Exception e){
	//			e.printStackTrace();
	//		}
	//
	//		//BitmapFactory.Options options = new BitmapFactory.Options();
	//		//options.inSampleSize = chunks;
	//
	//		Bitmap temp = BitmapFactory.decodeStream(in ,null ,null);
	//		return temp;
	//	}

	public interface KeyClickListener {

		public void keyClickedIndex(String index);
	}
}