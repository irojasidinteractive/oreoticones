package co.imagendigital.oreo.oreoticones;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;


public class SplashScreenActivity extends Activity {

	private long splashDelay = 10000;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash_screen);

		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				Intent mainIntent = new Intent().setClass(SplashScreenActivity.this, MainActivity.class);
				startActivity(mainIntent);
				finish();
			}
		};

		Timer timer = new Timer();
		timer.schedule(task, splashDelay);
	}

}
