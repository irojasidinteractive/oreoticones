package co.imagendigital.oreo.oreoticones;

import com.parse.Parse;
import co.imagendigital.oreo.util.ParseConnect;
import android.app.Application;
import android.util.Log;

public class Oreo extends Application{

	public static final String TAG = "Oreoticones";
	public static final String TAG_TEMP = "IARL";

	static ParseConnect pc;


	@Override
	public void onCreate() {
		super.onCreate();
		initializeParseData();
		initializeData();
	}

	/**
	 * Data parse
	 */
	private void initializeParseData() {
		Log.i(TAG_TEMP, "Oreoticones initializeParseData");
		Parse.enableLocalDatastore(this);
		Parse.initialize(this, getResources().getString(R.string.parse_application_id), getResources().getString(R.string.parse_client_key));
	}
	
	/**
	 * Initialize variables and menus
	 */
	private void initializeData(){
		Log.i(TAG_TEMP, "Oreoticones initializeData");
		pc = new ParseConnect();
	}

}
