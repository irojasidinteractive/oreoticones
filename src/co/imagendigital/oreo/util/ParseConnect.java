package co.imagendigital.oreo.util;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

import co.imagendigital.oreo.oreoticones.EmoticonsGridAdapter;
import co.imagendigital.oreo.oreoticones.Oreo;
import co.imagendigital.oreo.oreoticones.content.Category;
import co.imagendigital.oreo.oreoticones.content.Content;
import co.imagendigital.oreo.oreoticones.content.Emoticon;

import com.parse.CountCallback;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;

public class ParseConnect {

	/**
	 * Constructor
	 */
	public ParseConnect() {
		Log.i(Oreo.TAG_TEMP, " ParseConnect");
		//		getNumberOfCategorty();
		getCategories();
		getEmoticons();
	}

	/** Get dreams for time line */
	private void getEmoticons(){
		ParseQuery<ParseObject> query = ParseQuery.getQuery("emoticons");
		query.findInBackground(findCallbackGetEmoticons);
	}

	//	/**
	//	 * Number of categories
	//	 */
	//	private void getNumberOfCategorty() {
	//		ParseQuery<ParseObject> query = ParseQuery.getQuery("Category");
	//		query.whereEqualTo("playerName", "Sean Plott");
	//		query.countInBackground(new CountCallback() {
	//		  public void done(int count, ParseException e) {
	//		    if (e == null) {
	//		    	Content.setNUMBER_OF_CATEGORY(count);
	//		    } else {
	//		    	Content.setNUMBER_OF_CATEGORY(0);
	//		    }
	//		  }
	//		});
	//	}

	/** Get dreams for time line */
	private void getCategories(){
		ParseQuery<ParseObject> query = ParseQuery.getQuery("Category");
		query.whereEqualTo("enable", true);
		query.orderByAscending("Order");
		query.findInBackground(findCallbackGetCategories);
	}

	/** callback for method get emoticons */
	private FindCallback findCallbackGetEmoticons = new FindCallback<ParseObject>() {
		public void done(List<ParseObject> objects, ParseException e) {
			Log.i(Oreo.TAG_TEMP, " findCallbackGetDreams");

			if (e == null) {
				Log.i(Oreo.TAG_TEMP, " findCallbackGetDreams ok");
				findCallbackGetEmoticons(objects);
			}else{
				Log.i(Oreo.TAG_TEMP, " findCallbackGetDreams error ");
				Log.e(Oreo.TAG_TEMP, " findCallbackGetDreams error "+e);
			}
		}
	};
	/** callback for method get emoticons */
	private FindCallback findCallbackGetCategories = new FindCallback<ParseObject>() {
		public void done(List<ParseObject> objects, ParseException e) {

			if (e == null) {
				findCallbackGetCategories(objects);
			}else{
				Log.i(Oreo.TAG_TEMP, " findCallbackGetDreams error ");
				Log.e(Oreo.TAG_TEMP, " findCallbackGetDreams error "+e);
			}
		}
	};

	/** 
	 * Set emoticons of timeline in local storage 
	 * @param objects
	 */
	private void findCallbackGetEmoticons(List<ParseObject> objects) {
		for (int i = 0; i < objects.size(); i++) {

			ParseFile bum 		= (ParseFile) objects.get(i).get("emoticonImage");
			ParseFile bumIcon 	= (ParseFile) objects.get(i).get("emoticonIcon");
			String url 			= null;
			String urlIcon 		= null;
			String name 		= null;
			if (bum != null){ 
				url = bum.getUrl();
				name = bum.getName();
			}
			if (bumIcon != null){ 
				urlIcon = bumIcon.getUrl();
			}
			ArrayList<String> listCategories = new ArrayList<String>();     
			
			JSONArray jArray = objects.get(i).getJSONArray("objectIdCategory"); 
			
			try {
				if (jArray != null) { 
					
					for (int k = 0; k < jArray.length(); k++){ 
						listCategories.add(jArray.get(k).toString());
					} 
				} 																									
				final Emoticon emoticon = new Emoticon(objects.get(i).getObjectId(), name, url, objects.get(i).getString("description"), objects.get(i).getString("fileType"), listCategories, urlIcon);
				Content.getEMOTICON_LIST().add(emoticon);
			} catch (JSONException e) { e.printStackTrace(); }
		}
	}
	/** 
	 * Set emoticons of timeline in local storage 
	 * @param objects
	 */
	private void findCallbackGetCategories(List<ParseObject> objects) {
		
		for (int i = 0; i < objects.size(); i++) {

			final Category category = new Category(objects.get(i).getObjectId(), objects.get(i).getString("nameCategory"));
			Content.getCATEGORY_LIST().add(category);
		}
	}
}
